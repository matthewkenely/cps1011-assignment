#include <stdlib.h> // For strtoll()
#include <string.h> // For strlen()

#include "problem_solving.h"

#define INLEN 3

void i();
void ii();
void iii();
void iv();
void v();

void menu();
int input();

int main()
{
    int choice;

    printf("Input your menu choice (Empty line to exit)\n");

    while (1)
    {
        menu();
        choice = input();

        switch (choice)
        {
        case -1:
            goto end;
            break;

        case 1:
            i();
            break;

        case 2:
            ii();
            break;

        case 3:
            iii();
            break;

        case 4:
            iv();
            break;

        case 5:
            v();
            break;
        }

        printf("\n");
    }

end:;
    printf("Exiting...\n");

    return 0;
}

void menu()
{
    printf(
          "1)   i. init_array()"
        "\n2)  ii. display()"
        "\n3) iii. reverse()"
        "\n4)  iv. frequency()"
        "\n5)   v. display_freq()"
        "\n");
}

int input()
{
    int choice;
    char str[INLEN], *eptr;
    char *strptr;

    while (1)
    {
        printf(">> ");
        str[INLEN];
        strptr = str;

        fgets(str, INLEN, stdin);

        // End case
        if (str[0] == '\n')
        {
            return -1;
        }

        if (str[strlen(str) - 1] == '\n')
        {
            str[strlen(str) - 1] = '\0';
        }

        // Input validation
        while (*strptr) // While dereferencing str does not return \0
        {
            if (!(*strptr >= 49 && *strptr <= 53))
            {
                printf("Invalid Input!\n");
                goto cont;
            }

            strptr++;
        }

        choice = (int)strtol(str, &eptr, 10);

        if (!(choice >= 1 && choice <= 5))
        {
            printf("Invalid Input!\n");
            goto cont;
        }

        fflush(stdin);

        return choice;

    cont:
        fflush(stdin);
    }
}

void i()
{
    int nums[I_LIM];
    int c;

    printf("Size of the list: %d\n\n", c = init_array(nums));

    printf("Your integers:\n");
    for (int i = 0; i < c; i++)
    {
        printf("%d\n", nums[i]);
    }
}

void ii()
{
    int nums[] = {
        24, 5, 10, 2, 80, 100
    };

    display(nums, sizeof(nums) / sizeof(nums[0]));
}

void iii()
{
    int arr1[III_LEN] = {
        10, 20, 5, 6, 
        7, 13, 24, 15
    };

    int arr2[III_LEN];

    reverse(arr1, arr2, III_LEN);

    for (int i = 0; i < III_LEN; i++)
    {
        printf("%d\n", arr2[i]);
    }
}

void iv()
{
    int nums[IV_LEN] = {
        1, 1, 3, 4,
        5, 2, 1, 6,
        12, 5, 2, 10};

    val_freq_pair pairs[IV_LEN];

    int unique = frequency(nums, pairs);

    for (int i = 0; i < unique; i++)
    {
        printf("{%d, %d}\n", pairs[i].val, pairs[i].freq);
    }
}

void v()
{
    int pairs[V_LEN][2] = {
        {1, 3},
        {4, 2},
        {5, 1},
        {6, 1},
        {7, 3},
        {2, 5},
    };

    display_freq(pairs);
}