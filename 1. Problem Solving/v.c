#include "problem_solving.h"

void display_freq(int pairs[][2])
{
    printf("{\n");

    printf("\t\"frequency pairs\": [");

    for (int i = 0; i < V_LEN; i++)
    {
        printf("\n\t\t{\n");

        printf("\t\t\t\"value\": \"%02d\",\n", pairs[i][0]);
        printf("\t\t\t\"frequency\": \"%02d\",\n", pairs[i][1]);

        if (pairs[i + 1][0] == '\0')
        {
            printf("\t\t},\n");
            break;
        }

        else
        {
            printf("\t\t}\n");
        }
    }

    printf("\t]\n");

    printf("}\n");
}
