#include "problem_solving.h"

void reverse(int arr1[], int arr2[], int size)
{
    for (int c = size - 1, i = 0; c >= 0; c--, i++)
    {
        arr2[i] = arr1[c];
    }
}
