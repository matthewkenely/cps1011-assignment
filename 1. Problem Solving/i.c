#include <stdlib.h> // For strtoll()
#include <string.h> // For strlen()
#include <limits.h> // For INT_MIN and INT_MAX

#include "problem_solving.h"

int init_array(int nums[])
{
    // This clears out all values in nums prior to user input
    
    for (int i = 0; i < I_LIM; i++)
    {
        nums[i] = '\0';
    }

    long long input;
    int c;
    char str[12];
    char *eptr; // String buffer used for input validation, later converted to integer
    char *strptr;

    c = 0;

    printf("Please input up to %d integers line by line (Empty line to exit):\n", I_LIM);

    while (c < I_LIM)
    {
        strptr = str;

        printf(">> ");
        fgets(str, 12, stdin);

        // End case
        if (str[0] == '\n')
        {
            break;
        }

        if (str[strlen(str) - 1] == '\n')
        {
            str[strlen(str) - 1] = '\0';
        }

        int first = 1;

        // Input validation
        while (*strptr) // While dereferencing str does not return \0
        {
            if (
                !(
                    (*strptr >= 48 && *strptr <= 57) 
                    || *strptr == 43 
                    || *strptr == 45
                )
            )
            {
                printf("Invalid Input - Non-numeric characters!\n");
                goto cont;
            }

            if (!first)
            {
                if (!(*strptr >= 48 && *strptr <= 57))
                {
                    printf("Invalid Input - Non-numeric characters!\n");
                    goto cont;
                }
            }

            strptr++;
            first = 0;
        }

        input = strtoll(str, &eptr, 10);

        if (!(input >= INT_MIN && input <= INT_MAX))
        {
            printf("Invalid Input - Input out of integer range!\n");
            goto cont;
        }

        nums[c] = input;
        c++;

    cont:
        fflush(stdin);
    }

    return c;
}
