#include <stdio.h>

#define I_LIM 200
#define III_LEN 8
#define IV_LEN 12
#define V_LEN 7

typedef struct
{
    int val;
    int freq;
} val_freq_pair; // For iv.

// i.
int init_array(int nums[]);

// ii.
void display(int nums[], int size);

// iii.
void reverse(int arr1[], int arr2[], int size);

// iv.
int frequency(int nums[], val_freq_pair pairs[]);

// v.
void display_freq(int pairs[][2]);