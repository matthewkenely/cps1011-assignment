#include "problem_solving.h"

void display(int nums[], int size)
{
    printf("{\n");

    printf("\t\"array\": [");

    for (int i = 0; i < size; i++)
    {
        printf("\n\t\t{\n");

        printf("\t\t\t\"offset\": \"%02d\",\n", i);
        printf("\t\t\t\"value\": \"%02d\"\n", nums[i]);

        if (!(i + 1 == size))
        {
            printf("\t\t},\n");
        }

        else
        {
            printf("\t\t}\n");
        }
    }

    printf("\t]\n");

    printf("}\n");
}
