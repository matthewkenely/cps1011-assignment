#include "problem_solving.h"

int frequency(int nums[], val_freq_pair pairs[])
{
    int count;
    int displacement = 0; // Cater for non-uniqueness of elements
    int i;

    for (i = 0; i < IV_LEN; i++)
    {
        for (int x = 0; x < IV_LEN; x++)
        {
            if (pairs[x].val == nums[i])
            {
                displacement++;
                goto cont; // Don't append this value to pairs as it is already present
            }
        }

        count = 1;

        for (int j = i + 1; j < IV_LEN; j++)
        {
            if (nums[j] == nums[i])
            {
                count++;
            }
        }

        pairs[i - displacement].val = nums[i];
        pairs[i - displacement].freq = count;

    cont:;
    }

    return i - displacement;

    /*
    i:                number of elements in the array

    displacement:     number of times an integer which was already a 
                      value in val_freq_pair in pairs was encountered

    i - displacement: number of unique elements in the array
    */
}
