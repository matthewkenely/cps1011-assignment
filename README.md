# CPS1011 Assignment
## Matthew Kenely
#### Faculty of ICT B.Sc. IT (Hons) (AI)

***



  - [Project Description](#project-description)
  - [Repository Structure](#repository-structure)
    - [1. Problem Solving](#1-problem-solving)
    - [2. A DataTable Library](#2-a-datatable-library)

***

## Project Description
This repository was made as part of an assignment within the University of Malta's Faculty of ICT - CPS1011 Programming Principles in C

***

## Repository Structure
The two tasks, "**1. Problem Solving**" and "**2. A DataTable Library**" are each given a corresponding directory:<br>
```
"./1. Problem Solving"
"./2. A DataTable Library"
```
within which one may find the source code and <a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/1.%20Problem%20Solving/CMakeLists.txt">```CMakeLists.txt```</a> required for that task.

### **1. Problem Solving**
The functions to be implemented in part **(a)** are present in C source files with corresponding names:<br>
```
i.c
ii.c
iii.c
iv.c
v.c
```

Part **(b)** is implemented in the following:
```
b.c
```
by means of combining the functions in part **(a)** with a main and menu function in <a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/1.%20Problem%20Solving/b.c">```b.c```</a>  through the header file:
```
problem_solving.h
```

within which required constants for the functions in part **(a)** are defined, the datatype struct ```val_freq_pair``` is created for use in (a) iv. and the functions specified in part **(a)** are prototyped.



### **2. A DataTable Library**
The three parts, **(a)**, **(b)** and **(c)** are each given a corresponding directory:<br>
```
"./a"
"./b"
"./c"
```

#### **(a)**
Part **(a)** is implemented by means of the source files:
```
DataTable.c
DataTableFunctions.c
DataTableFunctions.h
```

<a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/a/DataTable.c">```DataTable.c```</a> acts as a test driver, with ```main()``` calling the different functions prototyped in <a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/a/DataTableFunctions.h">```DataTableFunctions.h```</a> and defined in <a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/a/DataTableFunctions.c">```DataTableFunctions.c```</a>

<a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/a/DataTableFunctions.c">```DataTableFunctions.c```</a> defines the 7 functions associated with ```DataTable_t```’s

<a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/a/DataTableFunctions.h">```DataTableFunctions.h```</a> is the header file, defining constants and types, prototyping required functions and connecting <a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/a/DataTable.c">```DataTable.c```</a> and <a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/a/DataTableFunctions.c">```DataTableFunctions.c```</a>

#### **(b)**
Part **(b)** extends the functionality of **(a)**, expanding on the follow source files:
```
DataTableFunctions.c
DataTableFunctions.h
```

<a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/b/DataTableFunctions.c">```DataTableFunctions.c```</a> defines the 7 functions associated with ```DataTable_t```’s

<a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/b/DataTableFunctions.h">```DataTableFunctions.h```</a> is the header file, defining constants and types, and prototyping required functions.

#### **(c)**
Part **(c)** links the test driver:
```
DataTable.c
```
with a shared library compiled from the source files in **(b)**:
```
DataTableFunctions.c
DataTableFunctions.h
```
This library linkage is defined in the <a href="https://gitlab.com/matthewkenely/cps1011-assignment/-/blob/submission/2.%20A%20DataTable%20Library/CMakeLists.txt">```CMakeLists.txt```</a> of **2. A DataTable Library**