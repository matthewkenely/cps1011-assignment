/* DataTable operation functions */

#include <stdio.h>
#include <string.h> // For strtok()
#include <stdlib.h> // For malloc()
#include "DataTableFunctions.h"

/* --- Function implementation --- */

DataTable_t *initDT(labels_t labels)
{
    DataTable_t *table;
    table = malloc(sizeof(*table));

    table->loaded = 0;

    // Assign provided labels
    for (int i = 0; i < FLOAT_COLUMNS + STRING_COLUMNS; i++)
    {
        strcpy(table->labels[i], labels[i]);
    }

    // Allocate memory for 3 float columns containing CHUNK elements
    for (int i = 0; i < FLOAT_COLUMNS; i++)
    {
        (table->float_columns)[i] = calloc(CHUNK, sizeof(*((table->float_columns)[i])));
    }

    // Allocate memory for 3 string columns containing CHUNK elements
    for (int i = 0; i < STRING_COLUMNS; i++)
    {
        (table->string_columns)[i] = calloc(CHUNK, sizeof(*((table->string_columns)[i])));
    }

    return table;
}

void deinitDT(DataTable_t **table)
{
    // Free float columns
    for (int i = 0; i < FLOAT_COLUMNS; i++)
    {
        free(((*table)->float_columns)[i]);
        ((*table)->float_columns)[i] = NULL;
    }

    // Free string columns
    for (int i = 0; i < STRING_COLUMNS; i++)
    {
        free(((*table)->string_columns)[i]);
        ((*table)->string_columns)[i] = NULL;
    }

    // Free table
    free(*table);
    *table = NULL;
}

void loadDT(char path[], DataTable_t *table)
{
    if (table == NULL)
    {
        printf("|| Invalid table\n");
        return;
    }
    else
    {
        // Accomodate for projectDT()
        table->labels_str = 0;
        table->labels_end = COLUMNS - 1;

        table->element_count = 0;

        FILE *in;

        if ((in = fopen(path, "r")) == NULL)
        {
            printf("Error opening file!\n");
            return;
        }

        char *buffer;
        int bufsize = 1024;

        // Buffer for .csv lines
        buffer = malloc(bufsize * sizeof(*buffer));

        char *token;

        int i;
        int c = 0;

        // Read .csv line-by-line
        while (fgets(buffer, bufsize, in) && c < MAX_ROWS)
        {
            /* Dynamic memory allocation --> assign memory location for         */
            /* CHUNK more elements once assigned memory is going to be exceeded */
            if (table->element_count % CHUNK == 0)
            {
                // Allocate memory for CHUNK more floats
                for (i = 0; i < FLOAT_COLUMNS; i++)
                {
                    (table->float_columns)[i] = (realloc((table->float_columns)[i], ((table->element_count / CHUNK) + 1) * (CHUNK * sizeof(*((table->float_columns)[i])))));

                    if ((table->float_columns)[i] == NULL)
                    {
                        printf("|| Error assigning memory\n");
                        exit(EXIT_FAILURE);
                    }
                }

                // Allocate memory for CHUNK more strings
                for (i = 0; i < STRING_COLUMNS; i++)
                {
                    (table->string_columns)[i] = (realloc((table->string_columns)[i], ((table->element_count / CHUNK) + 1) * (CHUNK * sizeof(*((table->string_columns)[i])))));

                    if ((table->string_columns)[i] == NULL)
                    {
                        printf("|| Error assigning memory\n");
                        exit(EXIT_FAILURE);
                    }
                }
            }

            token = strtok(buffer, ";");

            for (i = 0; i < FLOAT_COLUMNS; i++)
            {
                // Convert token to double and assign to column as float_t
                (table->float_columns)[i][c] = strtod(token, NULL);

                token = strtok(NULL, ";");
            }

            // Assign all string columns except last
            for (i = 0; i < STRING_COLUMNS - 1; i++)
            {

                // Assign token to column as string_t
                // MAX_CHAR - 1 --> leave 1 byte for \0
                strncpy((table->string_columns)[i][c], token, MAX_STRING_CHARS - 1);

                token = strtok(NULL, ";");
            }

            /* Catch final character '\n' and replace with '\0' to */
            /* maintain consistency in data representation within  */
            /* the DataTable_t                                     */
            strncpy((table->string_columns)[i][c], token, strlen(token) - 1);

            (table->string_columns)[i][c][strlen(token) - 1] = '\0';

            c++;
            table->element_count++;
        }

        free(buffer);
        fclose(in);

        table->loaded = 1;
    }
}

void exportDT(DataTable_t *table, char path[])
{
    if (table == NULL || table->loaded == 0)
    {
        printf("|| Invalid table\n");
        return;
    }
    else
    {
        FILE *out;

        out = fopen(path, "w");

        // Accomodate for projectDT()
        int labels_str = table->labels_str;
        int labels_end = table->labels_end;

        int projecting_floats;
        int col_count;

        for (int j = 0; j < table->element_count; j++)
        {
            projecting_floats = 1;
            col_count = 1;

            // Use independent variable col_count to keep track of iteration as i is modified in the loop
            for (int i = labels_str; col_count <= labels_end - labels_str + 1; i++, col_count++)
            {
                if (projecting_floats && i > FLOAT_COLUMNS - 1)
                {
                    i -= FLOAT_COLUMNS;
                    projecting_floats = 0;
                }

                if (projecting_floats)
                {
                    // Export float column - last column
                    if (col_count == labels_end - labels_str + 1)
                    {
                        fprintf(out, "%f", table->float_columns[i][j]);
                    }
                    // Export float column
                    else
                    {
                        fprintf(out, "%f;", table->float_columns[i][j]);
                    }
                }
                else
                {
                    // Export string column - last column
                    if (col_count == labels_end - labels_str + 1)
                    {
                        fprintf(out, "%s", table->string_columns[i][j]);
                    }
                    // Export string column
                    else
                    {
                        fprintf(out, "%s;", table->string_columns[i][j]);
                    }
                }
            }

            // Print new-line after every line except the last one
            if (j != table->element_count - 1)
            {
                fprintf(out, "%s", "\n");
            }
        }
    }
}

void showDT(DataTable_t *table)
{

    if (table == NULL || table->loaded == 0)
    {
        printf("|| Invalid table\n");
        return;
    }
    else
    {
        // Accomodate for projectDT()
        int labels_str = table->labels_str;
        int labels_end = table->labels_end;

        // Show label names
        for (int i = labels_str; i <= labels_end; i++)
        {
            printf("%10.10s\t", table->labels[i]);
        }

        printf("\n");

        int projecting_floats;
        int col_count;

        // Show data
        // j < table->element_count --> accomodate for DataTable_t's with less than 10 elements
        for (int j = 0; j < 10 && j < table->element_count; j++)
        {
            projecting_floats = 1;
            col_count = 0;

            // Use independent variable col_count to keep track of iteration as i is modified in the loop
            for (int i = labels_str; col_count <= labels_end - labels_str; i++, col_count++)
            {
                if (projecting_floats && i > FLOAT_COLUMNS - 1)
                {
                    i -= FLOAT_COLUMNS;
                    projecting_floats = 0;
                }

                if (projecting_floats)
                {
                    printf("%10.1f\t", (table->float_columns)[i][j]);
                }
                else
                {
                    printf("%10.10s\t", (table->string_columns)[i][j]);
                }
            }

            printf("\n");
        }

        if (table->element_count > 10)
        {
            printf("\n");
            for (int i = labels_str; i <= labels_end; i++)
            {
                printf("%10.10s\t", ". . .");
            }

            printf("\n\n");

            projecting_floats = 1;
            col_count = 1;

            // Show last element of DataTable_t
            for (int i = labels_str; col_count <= labels_end - labels_str + 1; i++, col_count++)
            {
                if (projecting_floats && i > FLOAT_COLUMNS - 1)
                {
                    i -= FLOAT_COLUMNS;
                    projecting_floats = 0;
                }

                if (projecting_floats)
                {
                    printf("%10.1f\t", (table->float_columns)[i][table->element_count - 1]);
                }
                else
                {
                    printf("%10.10s\t", (table->string_columns)[i][table->element_count - 1]);
                }
            }

            printf("\n");
        }
    }

    printf("\n");
}

DataTable_t *projectDT(DataTable_t *table, int m, int n, int x, int y)
{
    if (table == NULL || table->loaded == 0)
    {
        printf("|| Invalid table\n");
        return NULL;
    }

    /* m < table->labels_str || n > table->labels_end -->   accomodate for */
    /* DataTable_t's on which projectDT() has already been performed           */
    if (m < table->labels_str || n > table->labels_end || m > n 
    || x < 0 || y > table->element_count - 1 || x > y)
    {
        printf("Invalid input\n");
        return NULL;
    }

    DataTable_t *new_table;
    new_table = initDT(table->labels);

    // New DataTable_t made usable in other functions despite not all columns containing values
    new_table->labels_str = m;
    new_table->labels_end = n;

    new_table->element_count = y - x + 1;

    // calloc() exact amount of memory required based on number of elements, y - x + 1
    for (int i = new_table->labels_str; i <= new_table->labels_end; i++)
    {
        (new_table->float_columns)[i] = calloc(y - x + 1, sizeof(*((new_table->float_columns)[i])));

        if ((new_table->float_columns)[i] == NULL)
        {
            printf("|| Error assigning memory\n");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < STRING_COLUMNS; i++)
    {
        (new_table->string_columns)[i] = calloc(y - x + 1, sizeof(*((new_table->string_columns)[i])));

        if ((new_table->string_columns)[i] == NULL)
        {
            printf("|| Error assigning memory\n");
            exit(EXIT_FAILURE);
        }
    }

    int projecting_floats;
    int col_count;

    // Copy values from original DataTable_t into new DataTable_t
    for (int j = x, new_j = 0; j <= y; j++, new_j++)
    {
        projecting_floats = 1;
        col_count = 0;

        // Use independent variable col_count to keep track of iteration as i is modified in the loop
        for (int i = m; col_count <= n - m; i++, col_count++)
        {
            if (projecting_floats && i > FLOAT_COLUMNS - 1)
            {
                i -= FLOAT_COLUMNS;
                projecting_floats = 0;
            }

            if (projecting_floats)
            {
                (new_table->float_columns)[i][new_j] = (table->float_columns)[i][j];
            }
            else
            {
                strcpy((new_table->string_columns)[i][new_j], (table->string_columns)[i][j]);
            }
        }
    }

    new_table->loaded = 1;
    return new_table;
}

void mutateDT(void (*f)(char my_string[64], double *my_float, int mutating_floats), DataTable_t *table, int col)
{
    if (table == NULL || table->loaded == 0)
    {
        printf("|| Invalid table\n");
        return;
    }

    // Accomodate for projectDT()
    if (!(col >= table->labels_str && col <= table->labels_end))
    {
        printf("|| Invalid column\n");
        return;
    }

    int mutating_floats = 1;

    /* Pass column type by means of boolean mutating_floats so user-defined function */
    /* can carry out different operations based on whether the column is of type     */
    /* float_t or string_t                                                           */
    if (col > FLOAT_COLUMNS - 1)
    {
        mutating_floats = 0;
        col -= FLOAT_COLUMNS;
    }

    if (mutating_floats)
    {
        for (int j = 0; j < table->element_count; j++)
        {
            (*f)(NULL, &(table->float_columns[col][j]), 1);
        }
    }
    else
    {
        for (int j = 0; j < table->element_count; j++)
        {
            (*f)(table->string_columns[col][j], NULL, 0);
        }
    }
}