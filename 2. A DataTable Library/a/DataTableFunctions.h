/* DataTable header file */


/* ----------------------------------- CONSTANTS ----------------------------------- */

#define CHUNK 50
#define CAT_BUF 5

#define FLOAT_COLUMNS 3
#define STRING_COLUMNS 3
#define COLUMNS STRING_COLUMNS + FLOAT_COLUMNS

#define MAX_ROWS 1000
#define MAX_STRING_CHARS 64


/* -------------------------------- TYPE DEFINITION -------------------------------- */

/* label (column name) type definition */
typedef char labels_t[COLUMNS][MAX_STRING_CHARS];

/* Column type definitions */
typedef double float_t;
typedef char string_t[MAX_STRING_CHARS];

/* DataTable_t struct */
typedef struct
{
    /* Metadata */
    int loaded;
    labels_t labels;
    int labels_str; // Accomodating for projectDT()
    int labels_end; // Accomodating for projectDT()
    
    int element_count;

    /* Data */
    float_t *float_columns[FLOAT_COLUMNS];    // Static number of float columns (3)
    string_t *string_columns[STRING_COLUMNS]; // Static number of string columns (3)
} DataTable_t;


/* ------------------------------ FUNCTION PROTOTYPES ------------------------------ */

/* operation:       initialise a DataTable_t.                                        */
/* preconditions:   a label_t array with the required label names and types      */
/*                  is passed as a parameter, along with the label count.          */
/* postconditions:  the DataTable_t pointed to is assigned minimal memory            */
/*                  to be worked on by loadDT().                                     */
DataTable_t *initDT(labels_t labels);


/* operation:       deinitialise a DataTable_t.                                      */
/* preconditions:   the address of a table pointer is passed as a parameter.         */
/* postconditions:  the DataTable_t pointed to by the pointer, its address passed    */
/*                  as a parameter, has its pointer attributes as well as the        */
/*                  table pointer itself freed and set to NULL so that their         */
/*                  previously assigned memory locations may be reused by malloc().  */
void deinitDT(DataTable_t **table);


/* operation:       load a .csv file into the DataTable_t pointed to by paramter     */
/* preconditions    the path to a valid .csv file is passed, as well as a pointer    */
/*                  to a DataTable_t on which initDT(), passed parameters            */
/*                  corresponding to the .csv, has been performed.                   */
/*                  The .csv must be delimited by semicolons (;) and column          */
/*                  elements must correctly correspond with the provided             */
/*                  label types.                                                   */
/* postconditions:  the DataTable_t pointed to by the pointer passed as a parameter  */
/*                  has its attributes allocated memory and populated according      */
/*                  to the provided .csv.                                            */
void loadDT(char path[], DataTable_t *table);


/* operation:       export the contents of a DataTable_t to a .csv file              */
/* preconditions:   a pointer to a valid DataTable_t on which initDT() and loadDT(). */
/*                  have been performed, as well as a valid .csv destination path    */
/*                  relative to the executable, are passed as parameters.            */
/* postconditions:  a .csv file is created at the provided destination.              */
void exportDT(DataTable_t *table, char path[]);


/* operation:       summarised display of the contents of a DataTable_t              */
/* preconditions:   a pointer to a valid DataTable_t on which initDT() and loadDT()  */
/*                  have been performed is passed as a parameter                     */
/* postconditions:  if the provided DataTable_t has more than 10 rows, the           */
/*                  first 10 rows as well as the last row are display in             */
/*                  stdout, else all its rows are displayed                          */
void showDT(DataTable_t *table);


/* operation:       projects part of/the entirety of a DataTable_t onto another      */
/*                  DataTable_t.                                                     */
/* preconditions:   a pointer to a valid DataTable_t on which initDT() and loadDT()  */
/*                  have been performed, as well as the column start and end index   */
/*                  and the row start and end index, m, n, x and y respectively and  */
/*                  all within the DataTable_t's range and in valid order, are       */
/*                  passed as a parameters                                           */
/* postconditions:  a pointer to the projected DataTable_t is returned, this         */
/*                  DataTable_t having labels_str and labels_end assigned        */
/*                  according to the values of m and n                               */
DataTable_t *projectDT(DataTable_t *table, int m, int n, int x, int y);


/* operation:       modifies a DataTable_t column according to a user-defined        */
/*                  function                                                         */
/*                  DataTable_t.                                                     */
/* preconditions:   the user-defined function must act within the allocated memory   */
/*                  buffer, CAT_BUF, if concatenating strings. This function's       */    
/*                  pointer, along with a pointer to a DataTable_t on which          */
/*                  initDT() and loadDT() have been performed as well as a valid     */
/*                  column number are passed  as parameters                          */
/* postconditions:  the specified DataTable_t column's element values are modified   */
void mutateDT(void (*f)(char my_string[64], double *my_float, int mutating_floats), DataTable_t *table, int col);
