/* DataTable driver */

#include <stdio.h>
#include <string.h>
#include "DataTableFunctions.h"

void my_func(char my_string[64], double *my_float, int mutating_floats);

int main()
{
    labels_t labels = {
            {"id"},
            {"telephone"},
            {"age"},
            {"name"},
            {"surname"},
            {"country"}
    };

    printf(">> Initialising DataTable A\n");
    DataTable_t *table_a = initDT(labels);

    char *path = "data_a.csv";
    printf(">> Loading ./%s into DataTable A\n", path);
    loadDT(path, table_a);

    printf("Contents of DataTable A:\n");
    showDT(table_a);

    printf(">> Projecting the first 5 elements of columns 2 - 4 of DataTable A into DataTable B\n", path);
    DataTable_t *table_b = projectDT(table_a, 2, 4, 0, 4);

    printf("Contents of DataTable B:\n");
    showDT(table_b);

    printf(">> Mutating column 3 of DataTable B\n");
    mutateDT(my_func, table_b, 3);

    printf("Contents of DataTable B:\n");
    showDT(table_b);

    char *new_path = "new_a.csv";
    printf(">> Exporting contents of DataTable B into ./%s\n", new_path);
    exportDT(table_b, new_path);

    printf(">> Deinitialising DataTable A\n");
    deinitDT(&table_a);
    showDT(table_a);

    return 0;
}

void my_func(char my_string[64], double *my_float, int mutating_floats)
{
    if (mutating_floats)
    {
        *my_float += 1000;
    }
    else
    {
        char new_string[CAT_BUF] = "_foo";

        strcat(my_string, new_string);
    }
}